import {createStore} from 'redux';

const ADD = "ADD";
const DELETE = "DELETE";

const addToDo = (text) => {
  return {
    type: ADD,
    text
  };
}

const deleteToDo = id => {
  return {
    type: DELETE,
    id
  };
}

const reducer = (state, action) => {
  const items = window.localStorage.getItem('react-redux-to-do');
  state = items ? JSON.parse(items) : [];
  var newState;

  switch (action.type) {
    case ADD:
      newState = [{ text: action.text, id: Date.now() }, ...state];
      window.localStorage.setItem('react-redux-to-do', JSON.stringify(newState));
      return newState;
    case DELETE:
      newState = state.filter(toDo => toDo.id !== action.id);
      window.localStorage.setItem('react-redux-to-do', JSON.stringify(newState));
      return newState
    default:
      return state;
  }
};

const store = createStore(reducer);

export const actionCreators = {
  addToDo,
  deleteToDo
}

export default store;